Departmental teaching workload planner
======================================

Features
--------

### Proposed

This purpose of this app is to visualise and automate the process of teaching planning for the next semester and to
check the real workload of the current/previous semester.

- show the list of courses planned for the next semester
    - show the coverage status
        - to be covered/resolved, all planned classes of the course need to have assigned teachers who have confirmed
          this assignment
            - some classes can have multiple teachers required, the number is unrestricted
- two roles: a teacher, an administrator
- a teacher sees all currently assigned classes and can express confirmation/rejection to each individual assignment
- federative login (auth.fit)
- data loaded from KosAPI
    - after the first load, any loaded change is emphasised
    - in-app override, any mismatch between in-app and KosAPI data is emphasised
- report of workload of external teachers
